import { IHoliday } from "../types/holiday.type";
export const holidayData:IHoliday[]=[
    {
        id:1,
        holidayName:"pongal",
        holidayType:"All",
        isAllHolidaySelected:true,
        isFloatHolidaySelected:false,
        isPlannedHolidaySelected:false
    },
    {
        id:2,
        holidayName:"Ugadi",
        holidayType:"All",
        isAllHolidaySelected:true,
        isFloatHolidaySelected:false,
        isPlannedHolidaySelected:false
    },
    {
        id:3,
        holidayName:"SriRamaNavamai",
        holidayType:"All",
        isAllHolidaySelected:true,
        isFloatHolidaySelected:false,
        isPlannedHolidaySelected:false
    },
    {
        id:4,
        holidayName:"Dusshera",
        holidayType:"All",
        isAllHolidaySelected:true,
        isFloatHolidaySelected:false,
        isPlannedHolidaySelected:false
    },
    {
        id:5,
        holidayName:"Diwali",
        holidayType:"All",
        isAllHolidaySelected:true,
        isFloatHolidaySelected:false,
        isPlannedHolidaySelected:false
    },
]


