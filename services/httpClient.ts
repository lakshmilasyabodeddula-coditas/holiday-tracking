import {holidayData } from "../holiday-data/holidayData";
import {of,pipe} from "rxjs";
import { Injectable } from "@angular/core";
@Injectable({
    providedIn: 'root'
  })
export class HttpClientService{
 get() {
     return of(holidayData).pipe();
}
}
