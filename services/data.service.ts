import { Injectable } from '@angular/core';
import { HttpClientService} from '../services/httpClient';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpclient:HttpClientService) { }
   getHolidays()
   {
    return this.httpclient.get();
   }
  }

