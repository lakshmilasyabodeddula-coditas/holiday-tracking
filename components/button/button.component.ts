import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
 
  @Input() value="";
  @Input() id!:string;
  @Output() onClick=new EventEmitter();
  constructor() { }

  shiftHoliday()
  {
    this.onClick.emit();
  }
  ngOnInit(): void {
  }

}
