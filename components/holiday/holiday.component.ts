import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IHoliday } from '../types/holiday.type';

@Component({
  selector: 'app-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.scss']
})
export class HolidayComponent implements OnInit {

  @Input() holidayList!:IHoliday[];
  @Output() shiftHolidayEvent=new EventEmitter();

  @Input() header="";
  @Input() button1!:string;
  @Input() button2!:string;
  
  data:any;
  toggleHoliday(id:number, button:string){
    this.data=[id,button];
     this.shiftHolidayEvent.emit(this.data);
  }
  constructor() { }
  ngOnInit(): void {
  }
  

}
