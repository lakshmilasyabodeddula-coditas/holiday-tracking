import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import { IHoliday } from './types/holiday.type';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'project';
  holidays!: IHoliday[];
  constructor(private data: DataService) { }
  get allHolidays() {
    return this.holidays.filter(holiday => holiday.isAllHolidaySelected === true);
  }
  get plannedHolidays() {
    return this.holidays.filter(holiday => holiday.isPlannedHolidaySelected === true);
  }
  get floatingHolidays() {
    return this.holidays.filter(holiday => holiday.isFloatHolidaySelected === true);
  }
  ngOnInit(): void {
    this.data.getHolidays().subscribe((response) => {
      this.holidays = response;
    })
  }
  toggleHoliday(data:any) {
    const selectedholidayId = data[0];
   const selectedholidayType = data[1];
    for (let holiday of this.holidays) {
      if (parseInt(selectedholidayId) === holiday.id) {
        if (selectedholidayType === "all") {
          holiday.isAllHolidaySelected = true;
          holiday.isFloatHolidaySelected = false;
          holiday.isPlannedHolidaySelected = false;
        }
        else if (selectedholidayType === "plan") {
          holiday.isPlannedHolidaySelected = true;
          holiday.isAllHolidaySelected = false;
          holiday.isFloatHolidaySelected = false;
        }
        else if (selectedholidayType === "float") {
          holiday.isPlannedHolidaySelected = false;
          holiday.isFloatHolidaySelected = true;
          holiday.isAllHolidaySelected = false;
        }
        break;
      }
    }
  }
}
