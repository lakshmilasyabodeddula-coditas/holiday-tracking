export interface IHoliday{
    id:number;
    holidayName:string,
    holidayType:string
    isAllHolidaySelected:boolean,
    isFloatHolidaySelected:boolean,
    isPlannedHolidaySelected:boolean
}